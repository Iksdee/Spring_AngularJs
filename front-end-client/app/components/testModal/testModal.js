import testModalComponent from './testModal.component';

export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('testModal', {
                url: '/testModal',
            });
    }).directive('testModal', testModalComponent);
}