class SellerDetailController {

    constructor($scope, $rootScope, $mdDialog, id, sellersService) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
        this.sellersService = sellersService;
        this.$scope.id = id;
        this.$scope.carList = null;
        this.initRecords();
    }

    initRecords() {
        this.sellersService.getSellerDetail(this.$scope.id).then(
            (data) => {
                this.$scope.carList = data.data;
            },
            () =>{
                console.log("Error connection with rest");
            }
        );
    }
    closeModal(){
        this.$mdDialog.cancel();
    }

}
SellerDetailController.$inject = ['$scope', '$rootScope','$mdDialog', 'id', 'sellersService'];
export default SellerDetailController;