import sellerDetailComponent from './sellerDetail.component';

export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('sellerDetail', {
                url: '/sellerDetail',
            });
    }).directive('sellerDetailComponent', sellerDetailComponent);

}