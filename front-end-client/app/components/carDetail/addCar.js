import addCarComponent from './addCar.component';
import addCarController from './addCar.controller'

export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('car/add', {
                url: '/car/add',
                template: '<add-car></add-car>',
                // template: require('../../components/carDetail/addCar.html')
                controller: addCarController
            });
    }).directive('addCar', addCarComponent);
}


