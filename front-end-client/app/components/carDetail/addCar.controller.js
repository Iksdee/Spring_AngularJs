class AddCarController {
    constructor($scope, $state, carsService) {
        this.$state = $state;
        this.$scope = $scope;
        this.carsService = carsService;
        this.car = null;
    }

    addCar(){
        this.carsService.saveCar(this.car).then(
            (success) => {
                this.$state.go('car');
                console.log('przekierowanie')
            },
            (failure) => {
                console.log('blad zapisu do bazy')
            }
        )
    }
}

AddCarController.$inject = ['$scope','$state', 'carsService'];
export default AddCarController;