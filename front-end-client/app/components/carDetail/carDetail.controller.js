const Product = 3;
class CarDetailController {


    constructor($scope, $rootScope, $mdDialog, id, carsService) {
       this.$scope = $scope;
       this.$rootScope = $rootScope;
       this.$mdDialog = $mdDialog;
       this.carsService = carsService;
       this.$scope.id = id;
       this.$scope.car = null;
       this.initRecords();
    }

    initRecords() {

        /*
        this.$rootScope.$on("Test", (event, test) =>{
            this.$scope.type = test.type;
           console.log(this.$scope.type);
        });*/

        this.carsService.getCarDetail(this.$scope.id).then(
            (data) => {
                this.$scope.car = data.data;
            },
            () =>{
                console.log("Error connection with rest");
            }
        );
    }
    closeModal(){
        this.$mdDialog.cancel();
    }

}
CarDetailController.$inject = ['$scope', '$rootScope','$mdDialog', 'id', 'carsService'];
export default CarDetailController;