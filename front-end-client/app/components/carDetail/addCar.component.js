import template from '../../components/carDetail/addCar.html';
import controller from './addCar.controller';

let addCarComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'addCtrl',
        bindToController: true
    };
};
export default addCarComponent;