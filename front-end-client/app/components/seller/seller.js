import sellerComponent from './seller.component';
import controller from './seller.controller';


export default app => {
    app.config(($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('seller', {
                url: '/seller',
                template: '<seller></seller>'
            })


    }).directive('seller', sellerComponent);

}
