import template from './car.html';
import controller from './car.controller';

let carComponent = function () {
  return {
    restrict: 'EA',
    scope: {},
    template: template,
    controller: controller,
    controllerAs: 'homeCtrl',
    bindToController: true
  };
};

export default carComponent;
