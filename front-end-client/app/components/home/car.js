import carComponent from './car.component';
import controller from './car.controller';


export default app => {
    app.config(($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('car', {
                url: '/car',
                template: '<car></car>'
            })
            .state('car/carDetails', {
                url: '/car/carDetail/{id}',
                resolve: {
                    car: ['carsService', '$stateParams', (carsService, $stateParams) =>
                        carsService.getCarDetail($stateParams.id)
                    ]
                }
            });

    }).directive('car', carComponent);

}
