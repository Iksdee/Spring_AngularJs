class RegisterController {

    constructor($scope, $state, registerService, $mdDialog, ngMessages ) {
        this.$state = $state;
        this.$scope = $scope;
        this.registerService = registerService;
        this.ngMessages = ngMessages;
        this.user = null;
        this.$mdDialog = $mdDialog;
        this.$scope.matchPattern = new RegExp(".{6,}");
    }

    registerUser() {
        console.log(this.user);
        if (this.user.password == this.user.passconfirm) {
            this.registerService.userRegister(this.user).then(
             (success) => {
                 this.$state.go('/login');
                 this.$scope.errorMessage = false;
             },
             (failure) => {
                 this.$scope.errorMessage = "This username is already in use";
             }
            )
        }else{
            this.$scope.errorMessage = "Password failure";
        }

    }



}

RegisterController.$inject = ['$scope','$state', 'registerService', '$mdDialog'];
export default RegisterController;
