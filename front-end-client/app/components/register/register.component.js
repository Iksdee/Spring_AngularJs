import template from '../../components/register/register.html';
import controller from './register.controller';

let registerComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'registerCtrl',
        bindToController: true
    };
};
export default registerComponent;