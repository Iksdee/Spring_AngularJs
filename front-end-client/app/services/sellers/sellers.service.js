class SellersService {

    constructor($http) {
        this.$http = $http;
        this.dataUrl = 'http://localhost:8082';
    }

    getSellerList(){
        return this.$http({
            method: 'GET',
            url: this.dataUrl + '/seller'
        });
    }

    getSellerDetail(id){
        return this.$http({
            method: 'GET',
            url: this.dataUrl + `/seller/${id}`
        });
    }
}
export default SellersService;



