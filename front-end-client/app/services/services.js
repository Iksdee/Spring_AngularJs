import carsService from './cars/cars';
import sellersService from './sellers/sellers';
import loginService from './users/auth'
import registerService from './users/reg'
import  pdfService from './pdf/pdf'

export default app => {
  INCLUDE_ALL_MODULES([carsService, sellersService, loginService, registerService, pdfService], app);
}
