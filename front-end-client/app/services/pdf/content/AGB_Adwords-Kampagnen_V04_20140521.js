// playground requires you to assign document definition to a variable called dd
export function start() {

    var termsAndConditions = [
        /*0*/{header: '§ 1 Allgemeines und Auftragsannahme', description: 'Diese AGB gelten für Verträge über Adwords-Kampagnen sowie für eine Veröffentlichung von Werbung (Textanzeigen aufgrund von Suchbegriffen) auf Internet-Portalen (insbesondere in Internet-Suchmaschinen). Vertragsabschlüsse mit Verbrauchern im Sinne des Konsumentenschutzgesetzes sind ausgeschlossen. Bestellungen werden persönlich durch Vertreter der HEROLD Business Data GmbH (in der Folge „HEROLD“), durch das Telesales Team der HEROLD telefonisch sowie online (www.herold.at) entgegengenommen. Die Vertreter von HEROLD sind nicht verpflichtet, die Zeichnungsberechtigung des Unterfertigers zu prüfen. Ein vom Besteller und einem Vertreter von HEROLD unterzeichnetes Bestellformular gilt als Angebot des Kunden, das HEROLD ohne Angabe von Gründen innerhalb von vier Wochen ab Unterzeichnung des Bestellformulars ablehnen darf. Dies gilt auch für telefonische Bestellungen und online Bestellungen; in diesem Fall läuft die für HEROLD bestehende Ablehnungsfrist vier Wochen ab der telefonischen Bestellung bzw. Zugang der online Bestellung. Eine von HEROLD allenfalls ausgestellte Bestätigung über die telefonische bzw. online Bestellung gilt nicht als Annahme des Angebots durch HEROLD. Das Angebot gilt erst als von HEROLD angenommen, wenn es nicht innerhalb der Ablehnungsfrist schriftlich (auch Fax und e-Mail) oder mündlich zurückgewiesen wurde oder noch vor Ablauf der Ablehnungsfrist durch Ausführung der Bestellung angenommen wurde. Zur Fristwahrung genügt bei mündlicher Ablehnung der Ausspruch innerhalb der Frist bzw. bei schriftlicher Ablehnung die rechtzeitige Absendung. Grundlage des Vertragsinhaltes ist ausschließlich die schriftliche Bestellung laut Bestellschein bzw. der Inhalt der Bestätigung bei einer telefonischen Bestellung bzw. online Bestellung. Mündliche Zusagen oder von diesen AGB abweichende Vereinbarungen werden nicht Vertragsinhalt.'},

        /*1*/{header: '\n§ 2 Auswahl der Suchmaschinen und Suchbegriffe, Gestaltung von Werbetexten, Freigabeprozess', description: 'Die Auswahl der Internet-Portale und Suchmaschinen, die Festlegung von Suchbegriffen sowie die Auswahl und Gestaltung des Anzeigentextes liegt im Ermessen von HEROLD. HEROLD wird nach Fertigstellung der Kampagne Informationen betreffend Suchbegriffen und Anzeigentext der Kampagne an die vom Kunden angegebenen E-Mail-Adresse übermitteln (in der Folge „Fertigstellungsbenachrichtigung“). Sollte der Kunde nicht innerhalb der von HEROLD genannten Frist Korrekturen oder einmalige Änderungswünsche zu den Anzeigentexten bekannt geben, gilt die Kampagne als freigegeben und werden diese entsprechend der Fertigstellungsbenachrichtigung veröffentlicht.'},

        /*2*/{header: '\n§ 3 Bereitstellung von Werbeinhalten', description: 'Für eine optimale Festlegung von Suchbegriffen und Gestaltung der Anzeigentexte ist die Bereitstellung von Inhalten durch den Besteller erforderlich. Der Besteller hat HEROLD daher über erstmaliges Verlangen alle für die Ausführung des Auftrages notwendigen Unterlagen rechtzeitig vorzulegen und HEROLD alle Informationen zu erteilen, die für eine Ausführung des Auftrages von Bedeutung sein können. Der Besteller bestätigt, dass sämtliche für die Auftragsdurchführung bereitgestellten Informationen, Texte und sonstige Unterlagen keine rechts- oder sittenwidrigen Inhalte aufweisen. Insbesondere bestätigt der Besteller, dass die bereitgestellten Texte keine Begriffe (insbesondere Marken, Unternehmens- oder Produktbezeichnungen, etc.) enthalten, deren Verwendung rechtswidrig wäre (siehe § 6). Der Besteller erteilt seine ausdrückliche Zustimmung, dass HEROLD die in Medien, insbesondere auf Websites, einschließlich Social Media Plattformen, veröffentlichten Daten, Firmenbezeichnungen, Logos und Marken zum Zwecke der Gestaltung von Anzeigentexten verwenden darf.'},

        /*3*/{header: '\n§ 4 Preise und Zahlungskonditionen', description: 'Die am Bestellschein bzw. der Auftragsbestätigung angegebenen Preise dürfen unverzüglich, dh noch vor Erbringung der vertragsgegenständlichen Leistung in Rechnung gestellt werden. Die Rechnungen sind binnen 7 Tagen ab Rechnungsdatum zur Zahlung fällig. Bei Verzug werden Verzugszinsen in der Höhe von 12% p.a. sowie Mahn- und Inkassospesen verrechnet. Sofern eine Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages, bzw. im Falle der Einzugsermächtigung bei nicht ausreichender Kontoabdeckung, sämtliche ausständigen Teilleistungen ohne weitere Nachfristsetzung fällig. HEROLD behält sich Preisänderungen vor. Während eines bestehenden Auftrages sind Preiserhöhungen möglich, wenn die Kosten für eine Anzeigenkampagne während des Auftrages angehoben werden. In diesem Fall ist der Besteller zu einer entsprechenden Nachzahlung verpflichtet. Gegen Forderungen von HEROLD kann nicht aufgerechnet werden. Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatzabhängig und wird sowohl bei der erst-maligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet.'},

        /*4*/{header: '\n§ 5 Laufzeit und Budgetierung', description: 'Die Vereinbarungen über Adwords-Kampagnen gelten für die am Bestellschein vereinbarte Vertragslaufzeit von drei, sechs oder 12 Monaten und können vor Ablauf der Vertragslaufzeit nicht gekündigt werden. Der Einsatz des Werbebudgets des Bestellers wird gleichmäßig über die Vertragslaufzeit verteilt, sofern keine abweichende Regelung getroffen wird. Sollte am Ende der Vertragslaufzeit Werbebudget noch nicht verbraucht worden sein, bleibt die Kampagne so lange bestehen, bis das gesamte Werbebudget aufgebraucht ist. Eine Rückzahlung des Werbebudgets an den Besteller ist ausgeschlossen.'},

        /*5*/{header: '\n§ 6 Verantwortlichkeit für Werbeinhalte', description: 'Der Besteller gewährleistet, dass sämtliche für die Auftragsdurchführung bereitgestellten Informationen, Texte und sonstige Unterlagen keine rechts- oder sittenwidrigen Inhalte aufweisen'},

        /*6*/{header: '', description: 'aufweisen (siehe auch § 3). Der Besteller gewährleistet zudem, dass er über sämtliche erforderlichen Nutzungs- und Bearbeitungsrechte sowie über das Recht zur Veröffentlichung von sämtlichen Unterlagen, einschließlich Logos, Marken, Unternehmens- und Produktbezeichnungen, Schriftzüge und Bilder, die der Besteller zur Veröffentlichung von Anzeigen zur Verfügung stellt oder die von HEROLD auftragsgemäß verwendet werden, verfügt. Der Besteller ist verpflichtet, HEROLD für sämtliche Schäden, die HEROLD aus der Veröffentlichung von rechts- oder sittenwidriger Inhalte entstehen, insbesondere für Schäden aufgrund von Ansprüchen Dritter, schad- und klaglos zu halten.'},

        /*7*/{header: '\n§ 7 Gewährleistung und Schadenersatz', description: 'Der Besteller ist zur Prüfung der Angaben in der Fertigstellungsbenachrichtigung verpflichtet. Allfällige Korrekturen sowie Änderungswünsche sind binnen der von HEROLD bekannt gegebenen Frist anzuzeigen, anderenfalls keine Gewährleistungs-und/oder Schadenersatzrechte geltend gemacht werden können. Die Gewährleistungsfrist endet sechs Monate ab Übermittlung der Fertigstellungsbenachrichtigung gemäß § 2. Schadenersatzansprüche sind binnen einer Frist von sechs Monaten ab Kenntnis des Schadens schriftlich geltend zu machen. HEROLD haftet ausschließlich für unmittelbare und typische Schäden, die von HEROLD nachweislich vorsätzlich oder grob fahrlässig verschuldet wurden; eine Haftung für leichte Fahrlässigkeit ist ausgeschlossen. Eine Haftung für Folgeschäden, insbesondere für entgangenen Gewinn, ist ausdrücklich ausgeschlossen. Die Höhe eines'
        + 'Schadenersatzanspruches ist mit der Höhe des Auftragswertes beschränkt. Sollte der Besteller seinen Mitwirkungspflichten gemäß § 3 nicht nachkommen, kann HEROLD für eine daraus resultierende Minderleistung nicht verantwortlich gemacht werden. Der Besteller nimmt zur Kenntnis, dass weder eine bestimmte Reihung noch die Erreichung eines bestimmten Geschäftserfolges von HEROLD geschuldet ist und daher nicht Gegenstand der Vereinbarung mit dem Besteller ist. Auch eine bestimmte Reihung bzw. Platzierung von Anzeigen kann seitens HEROLD nicht gewährleistet werden. Der Ausschluss von Mitbewerbern kann nicht vereinbart werden. HEROLD ist berechtigt, eine bereits begonnene Anzeigenkampagne zu beenden oder die Anzeigeninhalte sowie Suchbegriffe zu ändern, sollte sich für HEROLD der begründete Verdacht ergeben, dass die Anzeigeninhalte oder Suchbegriffe rechts- oder sittenwidrig sind. Für allfällige Leistungsstörungen des Betreibers des Internet- Portals, auf dem die Anzeige veröffentlicht wird, kann HEROLD nicht verantwortlich gemacht werden. Insbesondere hat HEROLD allfällige Offline-Zeiten nicht zu verantworten.'},

        /*8*/{header: '\n§ 8 Zustimmungserklärung Datenschutz- und Telekommunikationsgesetz', description: 'Mit Auftragserteilung erklärt sich der Besteller gemäß § 8 Abs. 1 Ziffer 2 DSG 2000 einverstanden, dass die am Bestellschein angegebenen Kundendaten erfasst und für Werbe- und Marketingzwecke von HEROLD verwendet werden. Der Besteller erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbeund Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit werblichen Informationen über HEROLD und über Kunden von HEROLD, zu erhalten. Weiters erteilt der Besteller seine ausdrückliche Zustimmung, dass HEROLD die in Medien, insbesondere auf Websites, einschließlich Social Media Plattformen, veröffentlichten Daten, Firmenbezeichnungen, Logos und Marken zum Zwecke der Gestaltung von Anzeigentexte verwenden darf. Diese Zustimmungserklärungen können jederzeit durch E-Mail an kundenservice@herold. at widerrufen werden und sind mangels Widerruf auch über das Vertragsverhältnis hinaus wirksam.'},

        /*9*/{header: '\n§ 9 Google Konto', description: 'HEROLD wird zur Abwicklung von Werbekampagnen ein Google Adwords Konto anlegen, welches von HEROLD für die Dauer der Werbekampagne betreut wird. Der Kunde hat kein Recht auf Zugriff auf das Google Adwords Konto, dies weder während, noch nach Beendigung der Vereinbarung.'},

        /*10*/{header: '\n§ 10 Belehrung KSchG', description: 'Sofern der Besteller Konsument im Sinne des Konsumentenschutzgesetzes („KSchG“) ist, ist er berechtigt, im Falle eines Haustürgeschäftes gemäß § 3 KSchG binnen einer Woche nach Zustandekommen des Vertrages (Unterfertigung des Bestellscheins) vom Vertrag zurückzutreten. Von einem im Fernabsatz abgeschlossenen Vertrag ist der Konsument im Sinne des KSchG berechtigt, innerhalb von sieben Werktagen zurück zu treten. Es genügt, wenn die Rücktrittserklärung innerhalb der Frist ausgesendet wird (§ 5e KschG). Die Rücktrittsfrist beträgt sieben Werktage, wobei der Samstag nicht als Werktag zählt. Sie beginnt bei Verträgen über die Erbringung von Dienstleistungen mit dem Tag des Vertragsabschlusses (Unterfertigung des Bestellscheins). Rücktrittserklärungen sind zu richten an kundenservice@herold.at.'},

        /*11*/{header: '\n§ 11 Sonstiges', description: 'Die allfällige Unwirksamkeit einzelner Bestimmungen dieser Allgemeinen Geschäftsbedingungen lässt die Geltung der restlichen Bestimmungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine wirksame Bestimmung, die ersterer nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt. Es gilt österreichisches Recht. Ausschließlicher Gerichtstand ist für beide Teile das in Handelssachen zuständige Gericht für den ersten Wiener Gemeindebezirk.'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},

                    ],
                    [
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms_bold'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header'},
                        {text: termsAndCons[11].description,   style: 'terms'},
                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen\n für Adwords-Kampagnen', fontSize: 15, alignment: 'right',  bold: false, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '',border: [false, false, false, false]},
                            {text: 'AGB Adwords-Kampagnen (V04_20140521)', fontSize: 6 , alignment: 'right',  bold: false, border: [false, false, false, false], margin: [0, 0, 0, 0]  }],
                        [{colSpan: 2, text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-0, Fax +43 (0) 2236/401-8, E-Mail: kundenservice@herold.at, www.herold.at, Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z, FB-Gericht : Landesgericht Wr. Neustadt', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},{}]
                    ]
                }, margin: [21,0]
            };

        },

        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 8.2,
                alignment: 'justify',
                // lineHeight: 2
            },
            terms: {
                fontSize: 8.2,
                alignment: 'justify'
            },
            terms_bold: {
                bold: true,
                fontSize: 8.2,
                alignment: 'justify',
                lineHeight: 1.1
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Calibri!!!
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;

}