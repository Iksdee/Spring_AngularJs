export function start() {

    var termsAndConditions = [
        /*0*/{header: '1. Allgemeines\n', description: '1.1 Diese Allgemeinen Geschäftsbedingungen gelten für Vereinbarungen über die Bereitstellung von personenbezogenen Daten mit HEROLD Business Data GmbH (in der Folge „HEROLD“) über das System MD Online. Ein Vertragsabschluss mit Konsumenten iSd Konsumentenschutzgesetzes ist ausgeschlossen. Allgemeine Geschäftsbedingungen des Vertragspartners kommen nicht zur Anwendung.'
        + '\n1.2 HEROLD ist berechtigt, durch Prüfung des Gewerbescheins bzw. Firmenbuchauszugs des Bestellers dessen Unternehmereigenschaft zu prüfen.'
        + '\n1.3 HEROLD ist berechtigt, Bestellungen ohne Angabe von Gründen innerhalb von vier Wochen ab Eingang des Bestellscheins in der Firmenzentrale abzulehnen. Der Auftrag gilt als von HEROLD angenommen, wenn er nicht innerhalb dieser Frist schriftlich (auch per Fax und E-Mail) oder mündlich zurückgewiesen wurde. Zur Fristwahrung genügt bei mündlicher Ablehnung der Ausspruch innerhalb der Frist bzw. bei schriftlicher Ablehnung die rechtzeitige Aufgabe zur Post.'
        + '\n1.4 Für die Annahme des Auftrags durch HEROLD ist allein die schriftliche Bestellung laut Bestellschein maßgeblich, mündliche Erläuterungen oder Zusagen werden keinesfalls Vertragsinhalt.'},

        /*1*/{header: '\n2. Vertragsgegenstand und Nutzungsregeln\n', description: '2.1  Vertragsgegenstand ist der Zugang zum Dienst MD Online für Privatdaten sowie der Erwerb einer nicht ausschließlichen Nutzungsbewilligung an bestimmten über den Dienst MD Online bereitgestellten Daten. Die Nutzungsbewilligung umfasst ausschließlich das Recht der Benutzung zur bestimmungsgemäßen Verwendung der Daten im Sinne dieser Allgemeinen Geschäftsbedingungen. Ein Anspruch des Lizenznehmers auf Lieferung des Quellcodes besteht nicht. Die Installation, Einweisung und Softwarepflege gehören nicht zum Leistungsumfang von HEROLD.'
        + '\n2.2  Der Lizenznehmer erwirbt eine Einfachlizenz (Zugang zu MD Online für einen Nutzer) oder eine Mehrfachlizenz für eine bestimmte Nutzeranzahl (gemäß Bestellschein, Vertrag bzw. Produktbeschreibung). Im Falle des Erwerbs einer Mehrfachlizenz dürfen immer nur höchstens so viele Zugriffsrechte auf MD Online vergeben werden, wie Lizenzen erworben wurden. Der Lizenznehmer hat dafür zu sorgen, dass die Benutzung nur im Rahmen der erworbenen Lizenz erfolgt.'
        + '\n2.3  Abfragen müssen unter Verwendung der zur Verfügung gestellten Benutzeroberfläche erfolgen. Neben den gesetzlichen Verboten ist bei allen HEROLD-Produkten die Verwendung im Zusammenhang mit der gewerblichen Adressenverwertung, zum Aufbau oder zur Ergänzung von Teilnehmer-, Firmen- oder anderer Datenbanken jeder Art und in jeder medialen Form (in Printform, elektronisch, auf CD-ROM, etc.), zur Durchführung eines Auskunftsdienstes oder eines Call-Centers, zur Erteilung von Telefonauskünften, zum Aufbau von Konkurrenzprodukten zu HEROLD-Produkten (insbesondere HEROLD CD-ROM bzw. DVD, HEROLD Intranet-Versionen, HEROLD Einzeladressenverkauf etc.), für nicht in direktem Zusammenhang mit eigenen Werbemaßnahmen stehende Anwendungen, die Verwendung zu sonstigen kommerziellen Zwecken sowie generell die Verwendung für Zwecke und im Interesse Dritter, wie auch die entgeltliche oder unentgeltliche Überlassung an Dritte verboten. Der Lizenznehmer hat den Zugang zu MD Online ausreichend gegen einen unbefugten Zugriff Dritter zu schützen.'
        + '\n2.4  Die über MD Online bereitgestellten Daten dürfen zur Durchführung von Marketing- und Werbemaßnahmen für Zwecke des vom Lizenznehmer angegebenen Unternehmensstandortes verwendet werden. Eine Verwendung für Zwecke Dritter – somit auch für Zwecke verbundener Unternehmen oder für andere Standorte desselben Unternehmens – ist untersagt. Die Nutzung des Zugangs zu MD Online darf nur an und für einen Unternehmensstandort des Lizenznehmers erfolgen. Auch die Mitbenutzung des Zugangs sowie daraus bezogener Daten und erstellter oder ergänzter Datenbanken durch andere Unternehmensstandorte oder durch verbundene Unternehmen ist unzulässig. Die Verwendung an bzw. für mehrere Unternehmensstandorte ist nur durch Erwerb von Filial-Lizenzen (samt der erforderlichen Anzahl von Zugriffsrechten) zulässig. Bei Durchführung von Werbeaussendungen ist HEROLD Business Data GmbH als Auftraggeber der Ursprungsdatei anzugeben.'
        + '\n2.5  Der Lizenznehmer nimmt zur Kenntnis, dass die über MD Online bereitgestellten Daten binnen 14 Tagen zu verwenden und danach zu löschen sind. Marketing- und Werbemaßnahmen dürfen daher immer nur mit aktuellen und maximal 14 Tage vor Durchführung der Marketing- und Werbemaßnahme aus MD Online bezogenen Daten durchgeführt werden. Der Einsatz von älteren Daten hat zur Folge, dass gesetzlich vorgesehene Aktualisierungen nicht berücksichtigt werden können, was zu Ansprüchen Dritter gegen den Lizenznehmer führen kann.'
        + '\n2.6  Der Lizenznehmer hat sicherzustellen, dass bei Durchführung von Werbeaussendungen als Auftraggeber der benutzten Ursprungsdatei HEROLD Business Data GmbH genannt wird.'
        + '\n2.7  Der Lizenznehmer darf weder die Zugangsdaten zu MD Online noch die über den Zugang zu MD Online erhaltenen Daten an Dritte weitergeben oder Dritten (auch nicht bloß vorübergehend) überlassen.'
        + '\n2.8  An der von HEROLD über MD Online bereitgestellten Datenbank bzw. den bereitgestellten Daten bestehen Urheberrechte gemäß § 76 c ff UrhG durch HEROLD. Die Datenbank sowie alle Teile davon dürfen nur in dem vereinbarten Umfang genutzt werden. Der Lizenznehmer hat bei der Nutzung der überlassenen Daten die gesetzlichen Bestimmungen, insbesondere diejenigen des Datenschutzes und des Telekommunikationsgesetzes (insbesondere § 107 TKG 2003), in eigener Verantwortung zu beachten. Bei Aussendungen durch den Lizenznehmer ist zwingend die Datenquelle (HEROLD Business Data GmbH) zu nennen.'
        + '\n2.9  An allen von HEROLD zur Verfügung gestellten Programmen und dazugehörigen Dokumentationen verbleiben die Eigentums- und Urheberrechte bei HEROLD.'
        + '\n2.10  Zum Nachweis der missbräuchlichen Nutzung genügt die Vorlage einer der Kontrolladressen, welche für jede Adressenlieferung exklusiv generiert und in die Adresslieferung eingefügt sind. Für jeden Fall der missbräuchlichen Adressennutzung hat der Lizenznehmer an HEROLD eine dem richterlichen Kündigungsrecht nicht unterliegende Vertragsstrafe in Höhe des zehnfachen Entgelts für das jeweilige Vertragsjahr zu leisten. Die Geltendmachung eines darüber hinausgehenden Schadens bleibt HEROLD bei Vorliegen der gesetzlichen Voraussetzungen vorbehalten.'},

        /*2*/{header: '\n3. Retouren und Datenschutz\n', description: '3.1  Trotz laufender Aktualisierung und Überarbeitung der Daten wird keine Gewähr geleistet, dass zum Zeitpunkt der Lieferung an den Lizenznehmer sämtliche Daten richtig und vollständig sind.'},


        /*3*/{header: '', description: '3.2  Da die Anschriften aus öffentlichen Registern, Verzeichnissen und Eigenangaben aus Befragungsaktionen zusammengestellt werden, kann nicht gewährleistet werden, dass die Daten inhaltlich korrekt sind. Retouren (Rückläufer) sind aus diesem Grund unvermeidlich und stellen keinen Mangel dar. Solche unvermeidbare Retouren werden nicht vergütet.',
            bold: '3.3  Der Lizenznehmer ist bei jeder Verwendung der Daten verpflichtet, das Datenschutzgesetz und das',
            normal: 'Telekommunikationsgesetz zu beachten und HEROLD diesbezüglich schad- und klaglos zu halten.'},

        /*4*/{header: '\n4. Entgelt und Zahlungsbedingungen\n', description: '4.1  Die am Bestellschein bzw. der Auftragsbestätigung angegebenen Preise werden unverzüglich, dh noch vor Erbringung der vertragsgegenständlichen Leistung in Rechnung gestellt. Die Rechnungen sind binnen 7 Tagen ab Rechnungsdatum zur Zahlung fällig. Bei Verzug werden Verzugszinsen in der Höhe von 12% p.a. sowie Mahn- und Inkassospesen verrechnet. Sofern eine Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages, bzw. im Falle der Einzugsermächtigung bei nicht ausreichender Kontoabdeckung, sämtliche ausständigen Teilleistungen ohne weitere Nachfristsetzung fällig.'
        + '\n4.2  Das Entgelt wird entsprechend der Entwicklung des von der Statistik Österreich verlautbarten Verbraucherindex 2010 (VPI 2010) oder des an seine Stelle tretenden Index jährlich erhöht, wobei der durchschnittliche Indexwert des Kalenderjahres in dem der Auftrag erteilt wurde als Basiswert heranzuziehen ist. Darüber hinaus sind Preiserhöhungen generell bei Erhöhung der Selbstkosten (z.B. Anstieg der Lizenzgebühren, Datenpreise) auch während der Laufzeit möglich.'
        + '\n4.3  Die Aufrechnung mit Ansprüchen des Lizenznehmers ist nur zulässig, wenn diese Ansprüche von HEROLD schriftlich bestätigt oder rechtskräftig festgestellt wurden.'},

        /*5*/{header: '\n5. Rechte des Lizenznehmers bei Mängeln; Haftung\n', description: '5.1 Der Lizenznehmer hat durch zumutbare Untersuchungen feststellbare Mängel unverzüglich, längstens binnen einer Woche nach Anlieferung bzw. Download der Adressdaten, versteckte Mängel unverzüglich nach ihrer Entdeckung schriftlich anzuzeigen, wobei die Anzeige per Email ausreichend ist. Versäumt der Lizenznehmer eine ihn hiernach betreffende Frist und hat er das zu vertreten, so kann er wegen der entsprechenden Mängel keine Ansprüche gegen HEROLD geltend machen.'
        + '\n5.2  Ein zeitlich versetzter Einsatz der Adressen entbindet den Lizenznehmer nicht von der Verpflichtung zur zumutbaren Prüfung der Lieferungen bei deren Eingang beim Lizenznehmer, dies gilt insbesondere für',
            bold:'den Einwand, auf elektronischem Versandweg gelieferte Daten wären nicht einlesbar.'},

        /*6*/{header: '', description: '5.3  Der Zugang zu MD Online ist auf eine dauerhafte Benützung ausgelegt. HEROLD leistet jedoch keine Gewähr für Störungen des Zugangs zu MD Online. Insbesondere sind Ausfälle im Falle von Wartungen möglich und zulässig.'
        + '\n5.4  Bei rechtzeitig begründeter Mängelanzeige ist HEROLD binnen angemessener First verpflichtet, nach eigener Wahl entweder Ersatz zu liefern oder nachzubessern (Nacherfüllung).'
        + '\n5.5  HEROLD haftet für Schäden, die von HEROLD nachweislich vorsätzlich oder grob fahrlässig verschuldet wurden; eine Haftung für leichte Fahrlässigkeit ist ausgeschlossen. Weiters ist die Höhe des Schadenersatzanspruches mit der Höhe des Auftragswertes beschränkt. Die Haftung für Mangelfolgeschäden, sowie für entgangenen Gewinn, ist ausgeschlossen und ist auf vertragstypisch vorhersehbare Schäden begrenzt.'
        + '\n5.6  Die Frist zur Geltendmachung von Gewährleistungsbehelfen, die einvernehmlich auf Verbesserung beschränkt werden, beträgt sechs Monate ab Bereitstellung der Daten.'},

        /*7*/{header: '\n6. Laufzeit\n', description: '6.1  Es gilt die am Bestellschein oder sonst schriftlich vereinbarte Mindestlaufzeit (1, 2 oder 3 Jahre). Mit Beendigung der Vereinbarung dürfen die über den Dienst MD Online bezogenen Daten nicht mehr verwendet werden.'
        + '\n6.2  Eine Kündigung durch den Lizenznehmer ist erstmals zum Ende der jeweiligen Mindestvertragslaufzeit, dies unter Einhaltung einer Kündigungsfrist von drei Monaten möglich. Die Kündigung hat schriftlich zu erfolgen. HEROLD ist ohne Berücksichtigung der Mindestlaufzeit zur Kündigung unter Einhaltung einer Kündigungsfrist von drei Monaten berechtigt.'
        + '\n6.3  Darüber hinaus ist eine außerordentliche Kündigung aus wichtigem Grund jederzeit möglich. Ein wichtiger Grund liegt insbesondere dann vor, wenn der Lizenznehmer gegen die Pflichten dieser AGB verstößt.'},

        /*8*/{header: '\n7. Zustimmungserklärung\n', description: '7.1 Mit Auftragserteilung erklärt sich der Lizenznehmer gemäß § 8 Abs. 1 Ziffer 2 DSG 2000 einverstanden, dass seine für die Bearbeitung wesentlichen Daten erfasst und für Marketing- und Werbezwecke von HEROLD verwendet werden, dies auch über die Vertragsdauer hinaus. Der Besteller erklärt sich mit der Angabe seiner Telefonnummer und seiner elektronischen Postadresse einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbe- und Marketingzwecken zu erhalten. Diese Zustimmungserklärungen können jederzeit widerrufen werden.'},

        /*9*/{header: '\n8. Erfüllungsort; Gerichtsstand; Schriftform\n', description: '8.1  Erfüllungsort für alle Verpflichtungen ist Mödling.'
        + '\n8.2  Es gilt Österreichisches Recht unter Ausschluss des UN-Kaufrechts. Ausschließlicher Gerichtsstand ist für beide Teile das sachlich zuständige Gericht für den ersten Wiener Gemeindebezirk.'
        + '\n8.3  HEROLD ist berechtigt, diese Allgemeinen Geschäftsbedingungen jederzeit zu ändern. Die Allgemeinen Geschäftsbedingungen sind in der jeweils gültigen Fassung zum Zeitpunkt des Vertragsabschlusses anwendbar. HEROLD empfiehlt daher, die Allgemeinen Geschäftsbedingungen vor jedem Vertragsabschluss erneut zu lesen. Änderungen und Ergänzungen dieser Vereinbarung bedürfen der Schriftform. Dies gilt auch für die Vereinbarung vom Erfordernis der Schriftform abgehen zu wollen.'
        + '\n8.4  Die allfällige Unwirksamkeit einzelner Bestimmungen dieser AGB lässt die Geltung der restlichen Bestimmungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine wirksame Bestimmung, die ersterer nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt.'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},

                    ],
                    [
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[3].bold,   style: 'terms_header'},
                        {text: termsAndCons[3].normal,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},
                        {text: termsAndCons[5].bold,   style: 'terms_header'},
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description, style: 'terms_header'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description, style: 'terms'},

                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen\n für MD Online Consumer', fontSize: 15, alignment: 'right',  bold: false, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '',border: [false, false, false, false]},
                            {text: 'AGB MD Online Consumer(V02_15092014)', fontSize: 6 , alignment: 'right',  bold: false, border: [false, false, false, false], margin: [0, 0, 0, 0]  }],
                        [{colSpan: 2, text: 'HEROLD Medien Data GmbH, D-80339 München, Garmischer Straße 4/V, Telefon: +49 89 1792633-10, Fax: +49 89 921850-20, E-Mail: info@heroldmedia.com, www.heroldmedia.com, Amtsgericht München, HRB 209 490, UID: DE294716459, Oberbank Deutschland- Kto-Nr.: 1001387040, BLZ 70120700, BIC: OBKLDEMX, IBAN: DE11701207001001387040', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},{}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 7.5,
                alignment: 'justify',
                lineHeight: 1.2
            },
            terms: {
                fontSize: 7.5,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Calibri!!!
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}

