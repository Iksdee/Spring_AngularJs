import CarsService from './cars.service';

export default app => {
     app.service('carsService', CarsService);
}
