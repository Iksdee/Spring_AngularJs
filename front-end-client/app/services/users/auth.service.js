class AuthService {

    constructor($http, $rootScope, $q, OAuth) {
        this.$http = $http;
        this.OAuth = OAuth;
        this.$rootScope = $rootScope;
        this.$q = $q;
        this.dataUrl = 'http://localhost:8082/login';
    }

    authenticate(username, password) {
        let data = {
            username: username,
            password: password
        };

        let deferred = this.$q.defer();
        this.OAuth.getAccessToken(data).then(
            (success) => {
                this.$rootScope.$broadcast('AUTHENTICATION_EVENT', {
                    isAuthenticated: true,
                    currentSymbol: username
                });
                deferred.resolve();
            }, (error) => {
                deferred.reject();
            }
        );
        return deferred.promise;
    }
}
AuthService.$inject = ['$http', '$rootScope', '$q', 'OAuth'];
export default AuthService;