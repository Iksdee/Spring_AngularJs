import RegisterService from './register.service';
export default app => {
    app.service('registerService', RegisterService);
}