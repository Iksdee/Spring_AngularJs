import AuthService from './auth.service';
export default app => {
    app.service('loginService', AuthService);
}