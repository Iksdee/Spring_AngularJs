import template from './navbar.html';
import controller from './navbar.controller';

let navbarComponent = function () {
  return {
    controller,
    template,
    scope: {
      name: '@'
    },
    bindToController: true
  };
};
export default navbarComponent;
