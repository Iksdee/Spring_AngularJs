// Angular & Router ES6 Imports
import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import appComponents from './components/components.js';
import commonComponents from './common/components.js';
import appServices from './services/services.js';
import appFilter from  './filter/filter'
import appConfiguration from './app.config';
import ngMaterial from 'angular-material';
import {name as ngOAuth} from 'angular-oauth2';
import uiBootstrap from 'angular-ui-bootstrap';
import ngMessages from 'angular-messages';
import ngAnnotations from 'angular-annotations';

// Single Style Entry Point
import './index.scss';

const app = angular.module('app', [ngMaterial, angularUIRouter, ngOAuth, uiBootstrap, ngMessages]);

// Components Entrypoint
appComponents(app);

// Common Components Entrypoint
commonComponents(app);

// App Services Entrypoint
appServices(app);

// App Filter Entrypoint
appFilter(app);

// Router Configuration
// Components must be declared first since
// Routes reference controllers that will be bound to route templates.
// appConfiguration(app);



