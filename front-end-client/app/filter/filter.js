
import paginationFilter from './pagination';

export default app => {
    INCLUDE_ALL_MODULES([paginationFilter], app);
}