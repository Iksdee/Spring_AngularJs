export default app => {
    app.filter('paginationFilter', function () {

        return function paginationFilter(arr, start) {
            start = Number(start);
            return arr.slice(start);
        };
    });

    console.log('stop');
}