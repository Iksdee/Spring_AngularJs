package com.app.web.service;

import com.app.web.dto.CarDto;
import com.app.web.entity.Car;
import com.app.web.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    public List<CarDto> getCarList(){
        List<Car> carList = carRepository.findAll();
        List<CarDto> carDtoList = carList.stream().map(p -> getCarDto(p)).collect(Collectors.toList());
        return carDtoList;
    }

    public CarDto getCarById(Integer id){
        Car car = carRepository.getOne(id);
        CarDto carDto = getCarDto(car);
        return carDto;
    }

    public List<CarDto> getCarBySellerName(String name){
        List<CarDto> carDtoList = new ArrayList<>();

        for (int i=1; i<=carRepository.count(); i++){
            Car car = carRepository.getOne(i);
            if (car.getSeller().getCompanyName().equals(name)){
                CarDto carDto = getCarDto(car);
                carDtoList.add(carDto);
            }
        }
        return carDtoList;
    }

    public CarDto saveCar(CarDto carDto){
        Car car = getCar(carDto);
        Car savedCar = carRepository.save(car);
        CarDto savedCarDto = getCarDto(savedCar);
        return savedCarDto;
    }


    private CarDto getCarDto(Car car){
        ModelMapper modelMapper = new ModelMapper();
        CarDto carDto = modelMapper.map(car, CarDto.class);
        return carDto;
    }


    private Car getCar(CarDto carDto){
        ModelMapper modelMapper = new ModelMapper();
        Car car = modelMapper.map(carDto, Car.class);
        return car;
    }



}
