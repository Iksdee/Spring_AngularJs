package com.app.web.service;

import com.app.web.dto.SellerDto;
import com.app.web.entity.Seller;
import com.app.web.repository.SellerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SellerService {

    @Autowired
    private SellerRepository sellerRepository;

    public List<SellerDto> getSellerList(){
        List<Seller> list = sellerRepository.findAll();
        List<SellerDto> dtoList = list.stream().map(p -> getSellerDto(p)).collect(Collectors.toList());
        return dtoList;
    }

    public SellerDto getSellerById(Integer id){
        Seller seller = sellerRepository.getOne(id);
        SellerDto sellerDto = getSellerDto(seller);
        return sellerDto;
    }

    private SellerDto getSellerDto(Seller seller){
        ModelMapper modelMapper = new ModelMapper();
        SellerDto sellerDto = modelMapper.map(seller, SellerDto.class);
        return sellerDto;
    }

}
