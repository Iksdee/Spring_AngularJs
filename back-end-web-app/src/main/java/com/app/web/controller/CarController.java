package com.app.web.controller;

import com.app.web.dto.CarDto;
import com.app.web.entity.Car;
import com.app.web.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {

    @Autowired
    private CarService carService;

    //@CrossOrigin(origins = "http://localhost:8090")
    @RequestMapping
    public ResponseEntity<?> getCarList() {
        List<CarDto> products = carService.getCarList();
        return new ResponseEntity<Object>(products, HttpStatus.OK);
    }

    //@CrossOrigin(origins = "http://localhost:8090")
    @RequestMapping(value = "/{id}")
    public ResponseEntity<?> getCarDetail(@PathVariable(value ="id") Integer id) {
       CarDto carDto = carService.getCarById(id);
        return new ResponseEntity<Object>(carDto, HttpStatus.OK);
    }


    //@CrossOrigin(origins = "http://localhost:8090")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createCar(@RequestBody CarDto carDto){
        CarDto createdCar = carService.saveCar(carDto);
        return new ResponseEntity<>(createdCar, HttpStatus.CREATED);
    }

}
