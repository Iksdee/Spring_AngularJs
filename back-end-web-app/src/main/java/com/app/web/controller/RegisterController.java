package com.app.web.controller;


import com.app.web.dto.UserDto;
import com.app.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody UserDto userDto) {
        UserDto check = userService.isUserInDB(userDto);
        if(check == null){
            UserDto createdUser = userService.saveUser(userDto);
            return new ResponseEntity<>(createdUser, HttpStatus.CREATED);
        }else if (check.getUsername().equals(userDto.getUsername())) {
            return new ResponseEntity<>("Username already exists!",  HttpStatus.NOT_MODIFIED);
        }else if (check.getEmail().equals(userDto.getEmail())){
            return new ResponseEntity<>("Account with that email already exists", HttpStatus.NOT_MODIFIED);
        }else{
            return new ResponseEntity<>("Unexpected error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
