package com.app.web.controller;


import com.app.web.dto.CarDto;
import com.app.web.dto.SellerDto;
import com.app.web.service.CarService;
import com.app.web.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/seller")
public class SellerController {

    @Autowired
    private SellerService sellerService;
    @Autowired
    private CarService carService;

    @RequestMapping
    public ResponseEntity<?> getSellerList(){
        List<SellerDto> products = sellerService.getSellerList();
        return new ResponseEntity<Object>(products, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}")
    public ResponseEntity<?> getSellerCars(@PathVariable(value ="id") Integer id){
        SellerDto sellerDto  = sellerService.getSellerById(id);
        List<CarDto> carDto = carService.getCarBySellerName(sellerDto.getCompanyName());
        return new ResponseEntity<Object>(carDto, HttpStatus.OK);
    }
}
