#angular-spring-webpack-starter

## Description
Starter for web application using the following technologies

1. [Angular JS](https://angularjs.org)
2. [Babel for ES 6](https://babeljs.io/)
3. [PureCss](http://purecss.io)
4. [Font Awesome](https://fortawesome.github.io/Font-Awesome)
5. [Webpack](https://webpack.github.io)
6. [Spring Boot](http://projects.spring.io/spring-boot)
7. [Maven](https://maven.apache.org)
8. [Travis CI](https://travis-ci.org)
9. [Coveralls](https://coveralls.io)

## Installation Instructions

